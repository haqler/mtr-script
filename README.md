**Script to run mtr command for several IPs in background**
---

**Usage**:

	run_mtr.sh -mf|--mtr_flags -o|--output_file -i|--input_file -cf|--concurrent-traces

**Parameters**:

	-ct|--concurrent-traces   -   number of mtr traces that will run in a background.
	
	-mf|--mtr-flags           -   additional flags for mtr. Optional, by default it's only '--report'.
	
	-i|--input-file           -   input file with IPs. Mandatory.
	
	-o|--output-file          -   output file for report. Optional, by default it's mtr_out_`date`_IP in current directory.
	
	                              If user supplied output file name, it will looks like 'user_string'_$IP.
	                              
**Example**

./run_mtr.sh   -mf "--report --no-dns -c 10"   -o mtr_output_file   -i ip_list_file   -ct 20
