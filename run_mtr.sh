#!/bin/bash --
# Sat Jun 23 03:36:24 EDT 2018
# haqler

usage_print()
{
	# here document. Write text to output until stop word, in this case it's 'END'
	cat << "END"
Usage:
	run_mtr -mf|--mtr_flags -o|--output_file -i|--input_file -cf|--concurrent-traces

Parameters:
	-ct|--concurrent-traces   -   number of mtr traces that will run in a background.
	-mf|--mtr-flags           -   additional flags for mtr. Optional, by default it's '--report'.
	-i|--input-file           -   input file with IPs. Mandatory.
	-o|--output-file          -   output file for report. Optional, by default it's mtr_out_`date`_IP in current directory.
	                              If user supplied output file name, it will looks like 'user_string'_$IP.

END
}

if [ "$#" -lt 2 ]; then
	usage_print
	exit 1
fi


# process user parameters
while [ "$#" -ne 0 ]; do
	case "$1" in
		'-mf'|'--mtr-flags')
			MTR_FLAGS="$2"
			shift 2
			;;

		'-ct'|'--concurrent-traces')
			CONC_TR_NUM="$2"
			shift 2
			;;

		'-i'|'--input-file')
			INPUT_FILE="$2"
			shift 2
			;;

		'-o'|'--output-file')
			OUTPUT_FILE="$2"
			shift 2
			;;
		*)
			echo "Unknown parameter: $1"
			usage_print
			exit 1
			;;
	esac

done

MTR_FLAGS=${MTR_FLAGS:='--report'}
CONC_TR_NUM=${CONC_TR_NUM:=10}

if ! [ -f "$INPUT_FILE" ]; then
	echo "$INPUT_FILE is not file"
	exit 1
fi

if [ -z "$OUTPUT_FILE" ]; then
	OUTPUT_FILE_HEAD='./mtr_out_'`date +%Y%m%d`

else
	OUTPUT_FILE_HEAD="$OUTPUT_FILE"
	mkdir -p "$(dirname "$OUTPUT_FILE")"
fi

echo '================================='
echo 'mtr flags: '$MTR_FLAGS
echo 'input file: '$INPUT_FILE
echo 'output file: '$OUTPUT_FILE_HEAD'_$IP'
echo 'concurrent traces number: '$CONC_TR_NUM
echo '================================='

count=0
while IFS= read -r IP; do
	if [ -z "$IP" ]; then
		continue
	fi

	echo 'IP: '$IP
	OUTPUT_FILE="${OUTPUT_FILE_HEAD}_$IP"
	
	# run mtr
	mtr "$MTR_FLAGS" $IP >"$OUTPUT_FILE" 2>&1 &
	(( count++ ))
	if [[ $count = "$CONC_TR_NUM" ]]; then
		wait
		count=0
	fi

done < "$INPUT_FILE"

exit 0

